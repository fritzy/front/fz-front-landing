import EmberRouter from '@ember/routing/router';

import config from 'fz-front-landing/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('en', function () {
    this.route('legal-notices');
  });
  this.route('fr', function () {
    this.route('mentions-legales');
  });
});
