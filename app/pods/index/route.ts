import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type IntlConfigService from 'fz-front-landing/services/intl-config';
import type RouterConfigService from 'fz-front-landing/services/router-config';

export default class IndexLocaleBalancer extends Route {
  @service declare intlConfig: IntlConfigService;
  @service declare routerConfig: RouterConfigService;

  async beforeModel() {
    const route = this.routerConfig.indexRoute;

    if (route) {
      this.replaceWith(route);
    }
  }
}
