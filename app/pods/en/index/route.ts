import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type IntlConfigService from 'fz-front-landing/services/intl-config';

export default class En extends Route {
  @service declare intlConfig: IntlConfigService;

  async beforeModel() {
    await this.intlConfig.setLocale('en');
  }
}
