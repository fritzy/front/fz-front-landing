import IndexController from 'fz-front-landing/pods/-index/controller';

export default class FrIndexController extends IndexController {}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
  interface Registry {
    'fr-index-controller': FrIndexController;
  }
}
