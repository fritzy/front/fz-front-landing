import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type RouterService from '@ember/routing/router-service';
import type IntlService from 'ember-intl/services/intl';
import type RouterConfigService from 'fz-front-landing/services/router-config';

export default class IndexController extends Controller {
  @service declare intl: IntlService;
  @service declare router: RouterService;
  @service declare routerConfig: RouterConfigService;

  @action
  onLanguageChange() {
    const route = this.routerConfig.indexRoute;

    if (route) {
      this.router.transitionTo(route);
    }
  }
}
