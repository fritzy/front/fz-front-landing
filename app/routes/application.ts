import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

import type ApplicationService from 'fz-front-landing/services/application';
import type IntlConfigService from 'fz-front-landing/services/intl-config';

export default class ApplicationRoute extends Route {
  @service declare application: ApplicationService;
  @service declare intlConfig: IntlConfigService;

  async beforeModel() {
    await this.intlConfig.setLocale(this.intlConfig.userLocale);
  }
}
