import Service, { inject as service } from '@ember/service';

import fetch from 'fetch';

import type ApplicationService from './application';
import type IntlService from 'ember-intl/services/intl';

export type supportedLocalesType = typeof IntlConfigService.SUPPORTED_LOCALES[number];

export default class IntlConfigService extends Service {
  public static readonly SUPPORTED_LOCALES = ['en', 'fr'] as const;

  private static readonly TRANSLATIONS_PATHS = new Map<supportedLocalesType, string>([
    ['en', 'translations/en.json'],
    ['fr', 'translations/fr.json'],
  ]);

  private static readonly LOCALE_KEY = 'locale';

  @service declare application: ApplicationService;
  @service declare intl: IntlService;

  async setLocale(locale: supportedLocalesType) {
    if (!this.intl.exists('application.title', locale)) {
      const translationPath = IntlConfigService.TRANSLATIONS_PATHS.get(locale);

      if (!translationPath) {
        throw new Error(`Unsupported locale ${locale}`);
      }

      const translations = await fetch(`/${translationPath}`);
      const translationsAsJson = await translations.json();

      this.intl.addTranslations(locale, translationsAsJson);
    }

    this.intl.setLocale(locale);

    if (this.application.localStorageAvailable) {
      localStorage.setItem(IntlConfigService.LOCALE_KEY, locale);
    }
  }

  get #defautLocale() {
    if (this.application.localStorageAvailable) {
      const storedLocale = localStorage.getItem(IntlConfigService.LOCALE_KEY);

      if (storedLocale) {
        return storedLocale;
      }
    }

    const supportedLanguage = navigator.languages
      .map((l) => l.split('-')[0])
      .find((l) => (this.supportedLocales as readonly string[]).includes(l));

    return supportedLanguage ?? 'en';
  }

  get userLocale(): supportedLocalesType {
    const primaryLocale = this.intl.primaryLocale !== 'en-us' ? this.intl.primaryLocale : undefined;

    return (primaryLocale ?? this.#defautLocale) as 'en' | 'fr';
  }

  get supportedLocales() {
    return IntlConfigService.SUPPORTED_LOCALES;
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    'intl-config-service': IntlConfigService;
  }
}
