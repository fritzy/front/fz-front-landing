import Service, { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';

type routes = 'index' | 'legalNotices';

export default class RouterConfigService extends Service {
  @service declare intl: IntlService;

  private static readonly APP_ROUTES: ReadonlyMap<routes, ReadonlyMap<string, string>> = new Map([
    [
      'index',
      new Map([
        ['en', 'en'],
        ['fr', 'fr'],
      ]),
    ],
    [
      'legalNotices',
      new Map([
        ['en', 'en.legal-notices'],
        ['fr', 'fr.mentions-legales'],
      ]),
    ],
  ]);

  get indexRoute() {
    return this.#getRoute('index');
  }

  get legalNoticesRoute() {
    return this.#getRoute('legalNotices');
  }

  #getRoute(route: routes) {
    return RouterConfigService.APP_ROUTES.get(route)?.get(this.intl.primaryLocale);
  }
}
