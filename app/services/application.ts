import { tracked } from '@glimmer/tracking';
import Service, { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';

export default class ApplicationService extends Service {
  @service declare intl: IntlService;

  @tracked colorScheme: 'light' | 'dark' = 'light';

  localStorageAvailable = false;

  indexedDbAvailable = false;

  constructor() {
    super();
    this.testLocalStorageAvailable();
  }

  get darkMode() {
    return this.colorScheme === 'dark';
  }

  private testLocalStorageAvailable() {
    const test = 'test';

    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      this.localStorageAvailable = true;
    } catch (e) {
      this.localStorageAvailable = false;
    }
  }
}

// DO NOT DELETE: this is how TypeScript knows how to look up your services.
declare module '@ember/service' {
  interface Registry {
    'application-service': ApplicationService;
  }
}
