import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type RouterService from '@ember/routing/router-service';
import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-landing/services/application';
import type RouterConfigService from 'fz-front-landing/services/router-config';

export default class LegalNoticesContent extends Component {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare router: RouterService;
  @service declare routerConfig: RouterConfigService;

  applicationDate = new Date(2021, 9, 31);
  lastUpdatedDate = new Date(2021, 9, 31);

  get pageTitle() {
    return `${this.intl.t('components.legal-notices.content.title')} | ${this.intl.t('application.title')}`;
  }

  get title() {
    return this.intl.t('components.legal-notices.content.title');
  }

  @action
  onLanguageChange() {
    const route = this.routerConfig.legalNoticesRoute;

    if (route) {
      this.router.transitionTo(route);
    }
  }
}
