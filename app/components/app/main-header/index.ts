import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type RouterConfigService from 'fz-front-landing/services/router-config';

export default class AppMainHeader extends Component {
  @service declare routerConfig: RouterConfigService;
}
