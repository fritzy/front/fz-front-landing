import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type { BsNavbar } from 'ember-bootstrap';
import type IntlService from 'ember-intl/services/intl';
import type ApplicationService from 'fz-front-landing/services/application';
import type { supportedLocalesType } from 'fz-front-landing/services/intl-config';
import type IntlConfigService from 'fz-front-landing/services/intl-config';

interface AppMainHeaderLanguageSelectorArgs {
  nav: BsNavbar;
}

export default class AppMainHeaderLanguageSelector extends Component<AppMainHeaderLanguageSelectorArgs> {
  @service declare application: ApplicationService;
  @service declare intl: IntlService;
  @service declare intlConfig: IntlConfigService;

  @tracked opened = false;

  get selected() {
    return this.intl.t(`components.app.main-header.language-selector.${this.intl.primaryLocale}.value`);
  }

  @action
  onHide() {
    this.opened = false;
  }

  @action
  onShow() {
    this.opened = true;
  }

  @action
  languageOption(locale: supportedLocalesType) {
    return `blabla ${locale}`;
  }

  @action
  setLanguage(locale: supportedLocalesType) {
    this.intlConfig.setLocale(locale);
  }
}
