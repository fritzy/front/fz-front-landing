import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

import type { BsDropdownMenu } from 'ember-bootstrap';
import type IntlService from 'ember-intl/services/intl';
import type { supportedLocalesType } from 'fz-front-landing/services/intl-config';
import type IntlConfigService from 'fz-front-landing/services/intl-config';

interface AppMainHeaderLanguageSelectorOptionArgs {
  dropdownMenu: BsDropdownMenu;
  locale: supportedLocalesType;
}

export default class AppMainHeaderLanguageSelectorOption extends Component<AppMainHeaderLanguageSelectorOptionArgs> {
  @service declare intl: IntlService;
  @service declare intlConfig: IntlConfigService;

  get selected() {
    return this.intl.primaryLocale === this.args.locale;
  }

  get optionLabel() {
    return `${this.intl.t(
      'components.app.main-header.language-selector.' + this.args.locale + '.value',
    )} - ${this.intl.t('components.app.main-header.language-selector.' + this.args.locale + '.code')}`;
  }

  @action
  setLanguage(locale: supportedLocalesType) {
    this.intlConfig.setLocale(locale);
  }
}
