import Component from '@glimmer/component';

export default class LoaderRemover extends Component {
  constructor(owner: unknown, args: Record<string, never>) {
    super(owner, args);

    this.removeAppLoader();
  }

  private removeAppLoader() {
    const loader = document.querySelector('#app-loader');

    if (loader) {
      loader.remove();
    }
  }
}
