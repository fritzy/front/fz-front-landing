import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

import type IntlService from 'ember-intl/services/intl';

export default class IndexContent extends Component {
  @service declare intl: IntlService;

  get title() {
    return `${this.intl.t('components.index.title')} | ${this.intl.t('application.title')}`;
  }

  get simulatorUrl() {
    return `https://simulator.fritzy.finance/${this.intl.primaryLocale}`;
  }
}
