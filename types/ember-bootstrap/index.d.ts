declare module 'ember-bootstrap' {
  export interface BsDropdownMenu {}
  export interface BsForm {}

  export interface BsModal {
    close: () => void;
  }

  export interface BsNavbar {}
}
