'use strict';

const broccoliAssetRevDefaults = require('broccoli-asset-rev/lib/default-options');
const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const { addonConfigs } = require('./config/build/addons');
const { applyEnvironmentVariables, configureBabel, logWithAttention } = require('./config/build/ember-build');

const { CONCAT_STATS } = process.env;

module.exports = function (defaults) {
  const environment = EmberApp.env();
  const isProduction = environment === 'production';
  const isTest = environment === 'test';

  const env = {
    isProduction,
    isTest,
    environment,
    CONCAT_STATS,
  };

  const appOptions = {
    fingerprint: {
      enabled: isProduction,
      extensions: broccoliAssetRevDefaults.extensions.concat(['json']),
    },

    sourcemaps: {
      enabled: true,
    },

    tests: isTest, // Don't even generate test files unless a test build

    ...addonConfigs(env),
  };

  configureBabel(appOptions);
  applyEnvironmentVariables(appOptions);

  if (isProduction) {
    appOptions.autoImport = appOptions.autoImport || {};
  }

  logWithAttention(env, appOptions);

  const app = new EmberApp(defaults, appOptions);

  return app.toTree();
};
