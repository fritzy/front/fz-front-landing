import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';
import config from 'fz-front-landing/config/environment';
import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | App::Footer', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function (assert) {
    config.APP.version = '1.0.0';
    await render(hbs`<App::Footer />`);

    assert.dom('[data-test-version]').containsText('1.0.0');
  });
});
