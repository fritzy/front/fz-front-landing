import { render } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';

import hbs from 'htmlbars-inline-precompile';

module('Rendering | Component | App::LoaderRemover', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    await render(hbs`<div id="app-loader"></div>`);
    await render(hbs`<App::LoaderRemover />`);

    assert.dom('#app-loader').doesNotExist();
  });
});
