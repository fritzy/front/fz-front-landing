import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { setupIntl } from 'ember-intl/test-support';

module('Application | index', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /', async function (assert) {
    // When
    await visit('/');

    // Then
    assert.true(['/en', '/fr'].includes(currentURL()));
    assert.dom('.card').exists();
  });
});
