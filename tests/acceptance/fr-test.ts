import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import percySnapshot from '@percy/ember';
import { setupIntl } from 'ember-intl/test-support';
import config from 'fz-front-landing/config/environment';

module('Application | fr', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /fr', async function (assert) {
    await visit('/fr');

    assert.strictEqual(currentURL(), '/fr');
  });

  test('visual review on /fr', async function (assert) {
    config.APP.version = '1.0.0';
    await visit('/fr');

    assert.strictEqual(currentURL(), '/fr');

    await percySnapshot(assert as never);
  });
});
