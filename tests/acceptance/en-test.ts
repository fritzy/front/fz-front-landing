import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import percySnapshot from '@percy/ember';
import { setupIntl } from 'ember-intl/test-support';
import config from 'fz-front-landing/config/environment';

module('Application | en', function (hooks) {
  setupApplicationTest(hooks);
  setupIntl(hooks);

  test('visiting /en', async function (assert) {
    await visit('/en');

    assert.strictEqual(currentURL(), '/en');
  });

  test('visual review on /en', async function (assert) {
    config.APP.version = '1.0.0';
    await visit('/en');

    assert.strictEqual(currentURL(), '/en');

    await percySnapshot(assert as never);
  });
});
