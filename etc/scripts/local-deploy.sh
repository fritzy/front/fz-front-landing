#!/usr/bin/env bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
    cd $SCRIPT_PATH
fi
cd ../..

docker service rm fritzy_front-deploy
ansible-playbook -i etc/ansible/inventories/localhost etc/ansible/deploy.yml --extra-vars "environment_name=production"
