#!/usr/bin/env bash

SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
  cd $SCRIPT_PATH
fi
cd ../..

if [ -f .npmrc ]; then
  rm .npmrc
  echo "==========================="
  printf ".npmrc file deleted\n\n"
fi

if [ -f package.json ]; then
  sed -i '/"@fortawesome\/pro-light-svg-icons":/d' package.json
  echo "==========================="
  printf "package.json file edited\n\n"
fi

if [ -f config/icons.js ]; then
  sed -i '/pro-light-svg-icons/,/],/d' config/icons.js
  echo "==========================="
  printf "config/icons.js file edited\n\n"
fi

echo "==========================="
printf "installing deps\n\n"
yarn