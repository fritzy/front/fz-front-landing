# CHANGELOG

<!--- next entry here -->

## 0.6.0
2021-11-16

### Features

- add discord icon (8a0bb4d22c29e5a9478dc8267615e31bbe1a8776)
- add cloudflare analytics (e0e54a5c01a3af8462ee95d871b430faa4918d93)

### Fixes

- align socials icons (acbd859726ab225c6816b0ad286bb71ac7b27054)

## 0.5.1
2021-11-01

### Fixes

- footer margin (1992b9e985d9262a6f2ada5d4b21f72510203564)

## 0.5.0
2021-11-01

### Features

- add social icons (2588184d64f72bd19ed59daa8d65fbd50fdc7506)

## 0.4.0
2021-10-31

### Features

- update footer height (c0b09679df10dbc4f8ba39b5e4968c969f99d7e8)
- add legal notices (793345572568a4e75e164781b9f34be03c469ebd)

### Fixes

- link on brand in main-header (253fb2c54ecc2c5f11d30ec487d4724c139c9715)
- footer bare strings (d608f682c0c0b2298ee3927606f97df19d26a3fb)

## 0.3.1
2021-10-30

### Fixes

- simulator card on full width (ea22df3716d7027fdca9c64c7ae31cb46a0507cf)

## 0.3.0
2021-10-29

### Features

- dispatch index route (ed62215db1d849d066ce79601224439290e705d8)

### Fixes

- remove bare strings (6166e5f73d11d420cb7fc7a9f0b34c9a366f06a2)
- index title i18n (5ea6e702e77894adf3bb8d7d5f1769b0501c8d67)
- alphabetize translations (20f29330b763ae7b2fc6f200dfa2499d752b1e19)
- move main-header component to app::main-header (bce3f78290a6d24e4e95b121adb1d10de970aac2)

## 0.2.8
2021-10-17

### Fixes

- change description on main page (b4aedc8b35939f5c38b485f5b2c143b252c0c9d9)

## 0.2.7
2021-10-14

### Fixes

- configure redirects (41d7b58d6e3572ea1afbbd453411b57d2c3c5d4e)

## 0.2.6
2021-09-17

### Fixes

- add purge css (acca3271563962fa67b5b906b886b600ebb1f3be)
- remove unused files (3c8fe0c73f4ebd3b92c240b1433d172892cd92f0)

## 0.2.5
2021-09-17

### Fixes

- fix version of percy snapshot (75d0e781428b13220a1e324a311a13c34ffb836f)
- footer component (ea750f533e33a0e8f059af9d67f70fa837574824)
- add favicons (46d5dd3e5013323121e7681bf3038d3423a7d289)

## 0.2.4
2021-09-16

### Fixes

- add percy (3b07a41c44573049d9c0cac01d6b3aa2bec142ad)
- add percy chromium browser location (291f491044bbe6d7e3412d922bac612afbfcd459)

## 0.2.3
2021-09-16

### Fixes

- remove useless hamburger (83bb72c6b155da8cec08d2100ed22b4d07c541ac)

## 0.2.2
2021-09-16

### Fixes

- description on main page (f75caa7d481629f8c5a630347148b6a0275f06d9)

## 0.2.1
2021-09-13

### Fixes

- mistake (4f55494c3ffaa86a11a2dabfa56617f475f774b6)

## 0.1.2
2021-09-12

### Fixes

- add echo current version (57416863d6db859debc55a4ca3742ae7fe9fe283)
- gitlab pipeline trigger next version (bf8ae4cf5ae01e965499330c396c036c07f17105)
- gitlab next version variable (29599f701d0fe4629fe8f5ec9603bdcbe8ac4d41)
- commit release child (0cfdd7ce3ef596c0326fcd17dc84f4b6927a5d34)
- remove branch on child pipeline (dd760392f3f5fee0bf4575a5490bd85c2b1e18cb)
- commit-release syntax error (d8960053ceb1cdc53d5f019dee813960c542ec30)
- pass NEXT_VERSION to child pipeline (248f850e705a19d664649ad823d212fc353f2ee9)
- commit-release ci syntax error (f484a0ea53233be514e7c0cebcd870e0345868de)
- commit-release print next version (21273e63d95ae0aa3d69a1de0eb90cb9576ec25d)
- debug gitlab ci (f0746970b524934230e0bc21fb65deb2605cc35d)
- pass variable to commit-release pipeline (06d49f5c5a5900b196a302319758de8929281c75)
- conditional commut release (4f1ba47ff84f3d12a53ce2ca0b86f57af33c88c9)
- pass next version to child pipeline (bdb20dc384b585b92f2bf521d8134d434afe870e)

## 0.1.1
2021-09-11

### Fixes

- build pipeline (cf1dabd592801446a8ebde30ad7d917d8a046d94)
- gitlab pipeline (07638fb66537c5ca3321ab77c56e2b3eb1c10868)

## 0.1.0
2021-09-10

### Features

- initial template (602f3bf7514abd3d48d368c0f791a16c77407bff)