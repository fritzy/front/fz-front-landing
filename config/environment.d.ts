export default config;

/**
 * Type declarations for
 *    import config from 'fz-front-landing/config/environment'
 *
 * For now these need to be managed by the developer
 * since different ember addons can materialize new entries.
 */
declare const config: {
  environment: 'development' | 'production' | 'test';
  modulePrefix: string;
  podModulePrefix: string;
  locationType: string;
  rootURL: string;
  test: Record<string, unknown>;
  APP: { version: string; [key: string]: unknown };
};
