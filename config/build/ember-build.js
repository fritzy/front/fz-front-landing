/* eslint-disable no-console */
'use strict';

const path = require('path');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const { CONCAT_STATS, SOURCEMAPS_DISABLED, MINIFY_DISABLED } = process.env;

function applyEnvironmentVariables(appOptions) {
  if (SOURCEMAPS_DISABLED === 'true') {
    appOptions['sourcemaps'] = { enabled: false };
    appOptions.babel.sourceMaps = false;
  }

  if (MINIFY_DISABLED === 'true') {
    appOptions['ember-cli-terser'] = { enabled: false };
    appOptions.minifyCSS = { enabled: false };
  }

  if (CONCAT_STATS === 'true') {
    appOptions.autoImport = appOptions.autoImport || {};
    appOptions.autoImport.webpack = appOptions.autoImport.webpack || {};
    appOptions.autoImport.webpack.plugins = [
      new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        openAnalyzer: false,
        reportFilename: path.join(process.cwd(), 'concat-stats-for', 'ember-auto-import.html'),
      }),
    ];
  }
}

function configureBabel(appConfig) {
  appConfig.babel = appConfig.babel || {};
  appConfig.babel = {
    ...(appConfig.babel || {}),
    plugins: [
      ...(appConfig.babel.plugins || []),
      // for enabling dynamic import.
      require.resolve('ember-auto-import/babel-plugin'),
    ],
  };

  appConfig['ember-cli-babel'] = {
    ...(appConfig['ember-cli-babel'] || {}),
    enableTypeScriptTransform: true,
    throwUnlessParallelizable: true,
  };
}

function logWithAttention(...thingsToLog) {
  const longestLength = Math.max(thingsToLog.map((str) => str.length || 0)) || 120;

  const divider = '-'.repeat(longestLength);

  console.log(divider);

  thingsToLog.forEach((data) => console[typeof data === 'string' ? 'log' : 'dir'](data));

  console.log(divider);
}

module.exports = { applyEnvironmentVariables, configureBabel, logWithAttention };
