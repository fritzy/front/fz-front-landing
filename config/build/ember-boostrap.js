'use strict';

module.exports = {
  importBootstrapFont: false,
  bootstrapVersion: 4,
  importBootstrapCSS: false,
  whitelist: ['bs-navbar', 'bs-tooltip'],
};
