'use strict';

const { emberBootstrapContent } = require('ember-bootstrap-postcss/purgecss');
const emberBootstrap = require('./ember-boostrap');

function postcss(env) {
  const isDevelopment = !env.isProduction && !env.isTest;

  return {
    compile: {
      enabled: true,
      extension: 'scss',
      map: isDevelopment
        ? {
            inline: false,
            annotation: true,
            sourcesContent: true,
          }
        : false,
      parser: require('postcss-scss'),
      plugins: [
        {
          module: require('@csstools/postcss-sass'),
          options: {
            includePaths: ['node_modules'],
          },
        },
      ],
    },
    filter: {
      enabled: !isDevelopment,
      map: false,
      plugins: [
        {
          module: require('@fullhuman/postcss-purgecss'),
          options: {
            content: [
              './app/index.html',
              './app/components/**/*.hbs',
              './app/templates/**/*.hbs',
              './app/pods/**/*.hbs',

              './node_modules/admin-lte-ember/addon/**/*.hbs',
              './node_modules/admin-lte-ember/addon/**/*.ts',

              ...emberBootstrapContent(emberBootstrap.whitelist),
            ],
          },
        },
      ],
    },
  };
}

module.exports = { postcss };
