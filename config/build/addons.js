'use strict';

const { postcss } = require('./postcss');

function addonConfigs(env) {
  return {
    'asset-cache': {
      include: ['assets/**/*', 'translations/**/*'],
    },
    'ember-bootstrap': require('./ember-boostrap'),
    emberCliConcat: {
      js: { concat: false },
      css: { concat: true, preserveOriginal: false },
    },
    postcssOptions: postcss(env),
  };
}

module.exports = { addonConfigs };
