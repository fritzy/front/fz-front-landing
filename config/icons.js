module.exports = function () {
  return {
    'free-brands-svg-icons': ['discord', 'facebook-f', 'instagram', 'linkedin-in', 'twitter'],
    'free-solid-svg-icons': ['circle'],
  };
};
