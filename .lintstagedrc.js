'use strict';

module.exports = {
  'translations/**/*.json': (filenames) => filenames.map((filename) => `sort-json '${filename}'`),
};
